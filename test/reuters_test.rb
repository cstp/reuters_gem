require 'test/unit'
require 'reuters'
require 'vcr'

class ReuterTest < Test::Unit::TestCase

  VCR.configure do |c|
    c.cassette_library_dir = 'fixtures/vcr_cassettes'
    c.hook_into :webmock
    c.debug_logger = File.open('debug','w')
  end

  def setup
    test_login
  end

  def test_login
    VCR.use_cassette('login') do
      @client = Reuters::API.new(ENV['reuters_user'],ENV['reuters_pass'])
    end
  end

  def test_get_channel
    VCR.use_cassette('get_channel') do
      @client.get_channel(:us_top_news)
    end
  end

  def test_get_latest_2hours
    VCR.use_cassette('get_last_2hours') do
      @client.get_latest_packages(:us_top_news,"2h")
    end
  end

  def test_get_news
    VCR.use_cassette('get_news') do
      p @client.get_news_package(:us_top_news,
              @client.get_latest_packages(:us_top_news,"2h")[0])
    end
  end

end
