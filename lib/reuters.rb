require "reuters/version"

require 'rexml/document'
require 'net/https'
require 'net/http'
require 'cgi'
require 'nokogiri'

module Reuters

  class API
    AUTH_URL = 'commerce.reuters.com'
    SERVICE_URL = 'rmb.reuters.com'

    def initialize(username, password)
      auth(username,password)
    end

    def get_channel(chan_symbol)
      chan = channel_codes[chan_symbol]
      http_call("channels",{channel:chan})
    end

    def get_latest_packages(chan_symbol,maxAge)
      params = {maxAge:maxAge,channel:channel_codes[chan_symbol]}
      xml = http_call("packages",params)
      Nokogiri::XML(xml.to_s).xpath("//results/result/guid").map { |node|
        node.text
      }
    end

    def get_news_package(chan_symbol,package_id)
      params = {  channel:channel_codes[chan_symbol],
                  id: package_id }
      xml = http_call("package",params)
      headline = Nokogiri::XML(xml.to_s).xpath("//results/result/headline").map { |node|
        node.text
      }[0]

      text = Nokogiri::XML(xml.to_s).xpath("//results/result/mainLinks/link[mediaType='T']/guid").map { |link|
        {
          title: headline,
          body: get_news_body(chan_symbol,link.text)
        }
      }[0]
    end

    private

      def get_news_body(chan_symbol, id)
        params = {  channel:channel_codes[chan_symbol],
                    id: id,
                    entityMarkupField: "body"
        }
        xml = http_call("item",params)
        # XPATH is broken. I'm going in stupid!
        newsMessage = Nokogiri::XML(xml.to_s).xpath("node()")
        .select { |node| node.name == "newsMessage" }[0].xpath("node()")
        .select { |node| node.name == "itemSet" }[0].xpath("node()")
        .select { |node| node.name == "newsItem"}[0].xpath("node()")
        .select { |node| node.name == "contentSet"}[0].xpath("node()")
        .to_s
      end

      def auth(username,password)
        p = {"username"=>username, "password"=>password}
        doc = https_call('login',p)
        @auth_token = doc.elements.to_a("authToken")[0].text
      end

      def https_call(method, params={})
          http = Net::HTTP.new(AUTH_URL,443)
          http.use_ssl = true
          m = '/rmd/rest/xml/'+method

          call(method,http,m,params)
      end

      def http_call(method, params={})
          http = Net::HTTP.new(SERVICE_URL,80)
          http.use_ssl = false
          m = '/rmd/rest/xml/'+method
          params['token'] = @auth_token

          call(method,http,m,params)
      end

      def call(method,http,m,params={})
        body = ""
        http.start do |http|
          req = Net::HTTP::Get.new(m + '?'.concat(params.collect {|k,v| "#{k}=#{CGI::escape(v.to_s)}" }.join('&')))
          req.basic_auth("k_ellis","Reuters2")
          resp = http.request(req)
          body = resp.body
        end
        REXML::Document.new(body)
      end

      def channel_codes
        {
          us_top_news: "FES376"
        }
      end
  end
end
