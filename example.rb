#!/usr/bin/env ruby

require 'rexml/document'
require 'net/https'
require 'net/http'
require 'cgi'

class API
  AUTH_URL = 'commerce.reuters.com'
  SERVICE_URL = 'rmb.reuters.com'

  def initialize(username, password)
    p = {"username"=>username, "password"=>password}
    doc = self._call('login',p,true)
    @auth_token = doc.elements.to_a("authToken")[0].text
  end

  def call(method, params={})
    self._call(method, params, false)
  end

  def _call(method, params={}, is_auth=false)
    if is_auth
      http = Net::HTTP.new(AUTH_URL,443)
      http.use_ssl = true
      m = '/rmd/rest/xml/'+method
    else
      http = Net::HTTP.new(SERVICE_URL,80)
      http.use_ssl = false
      m = '/rmd/rest/xml/'+method
      params['token'] = @auth_token
    end
    body = ""
    http.start do |http|
      req = Net::HTTP::Get.new(m + '?'.concat(params.collect {|k,v| "#{k}=#{CGI::escape(v.to_s)}" }.join('&')))   
      req.basic_auth("k_ellis","Reuters2")
      resp = http.request(req)
      body = resp.body
    end
    return REXML::Document.new(body)
  end
end


api = API.new('stofferpedersen_API','')

# get all channels
doc = api.call("channels")
ch = doc.elements.to_a("//channelInformation")
puts 'CHANNEL LIST'
puts ''
ch.length.times do |i|
  puts ch[i].elements.to_a("alias")[0].text + '   ' + ch[i].elements.to_a("description")[0].text
end

# get items
doc = api.call("items",{"channel"=>"AdG977","channelCategory"=>"OLR","limit"=>"10"})
items = doc.elements.to_a("//result")
puts ''
puts ''
puts 'ITEM LIST'
puts ''
items.length.times do |i|
  puts items[i].elements.to_a("id")[0].text + '    ' + items[i].elements.to_a("headline")[0].text
end
