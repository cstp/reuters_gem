.PHONY: gem

gem:
	gem build reuters.gemspec
	gem install reuters-*.gem
