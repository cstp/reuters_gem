# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'reuters/version'

Gem::Specification.new do |spec|
  spec.name          = "reuters"
  spec.version       = Reuters::VERSION
  spec.authors       = ["Christoffer Pedersen"]
  spec.email         = ["stofferpedersen@gmail.com"]
  spec.description   = "A reuters gem"
  spec.summary       = "A gem for reuters"
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "vcr"
  spec.add_development_dependency "webmock"
  spec.add_development_dependency "nokogiri"
end
